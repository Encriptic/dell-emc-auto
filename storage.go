package dell_emc_auto

import (
	"encoding/gob"
	"errors"
	"os"
	"sync"
	"syscall"
)

type CarDataStorage interface {
	Sell(car Car) error
	Get(serialNumber uint64) (Car, error)
}

var (
	ErrCarAlreadySold       = errors.New("this car is already sold, sorry")
	ErrCarDoesntExist       = errors.New("this car doesn't exist")
	ErrStorageCannotBeEmpty = errors.New("storage cannot be empty")
)

type SimpleStorage struct {
	storage string

	mx  sync.RWMutex
	mem map[uint64]Car
}

func NewSimpleStorage(file string) (CarDataStorage, error) {
	if file == "" {
		return nil, ErrStorageCannotBeEmpty
	}

	f, fileErr := os.Open(file)
	if fileErr != nil {
		pathErr, ok := fileErr.(*os.PathError)
		if !ok || pathErr.Err != syscall.ENOENT {
			return nil, fileErr
		}
	}
	defer f.Close()

	mem := map[uint64]Car{}
	if fileErr != nil {
		return &SimpleStorage{
			storage: file,
			mem:     mem,
		}, nil
	}

	err := gob.NewDecoder(f).Decode(&mem)
	if err != nil {
		return nil, err
	}

	return &SimpleStorage{
		storage: file,
		mem:     mem,
	}, nil
}

func (s *SimpleStorage) Sell(car Car) error {
	s.mx.Lock()
	defer s.mx.Unlock()

	if _, ok := s.mem[car.SerialNumber]; ok {
		return ErrCarAlreadySold
	}
	s.mem[car.SerialNumber] = car

	f, err := os.Create(s.storage)
	if err != nil {
		return err
	}
	defer f.Close()

	err = gob.NewEncoder(f).Encode(&s.mem)
	if err != nil {
		return err
	}

	return f.Sync()
}

func (s *SimpleStorage) Get(serialNumber uint64) (Car, error) {
	s.mx.RLock()
	defer s.mx.RUnlock()

	car, ok := s.mem[serialNumber]
	if !ok {
		return car, ErrCarDoesntExist
	}

	return car, nil
}
