package dell_emc_auto

import (
	"encoding/json"
	"errors"
	"io"
	"log"
	"math"
	"net/http"
	"strconv"
)

type CarStoreHandler struct {
	SoldCars CarDataStorage
}

func (cs *CarStoreHandler) Sell(rw http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		rw.WriteHeader(http.StatusMethodNotAllowed)
		io.WriteString(rw, "incorrect method")
		return
	}

	body := &io.LimitedReader{
		R: r.Body,
		N: 10 * 1024 * 1024, // This should be enough for any sane data about the car
	}

	var soldCar Car
	err := json.NewDecoder(body).Decode(&soldCar)
	if err != nil {
		log.Println("failed to parse data about bought car, err is", err.Error())
		rw.WriteHeader(http.StatusBadRequest)
		io.WriteString(rw, "incorrect request")
		return
	}

	err = IsValid(&soldCar)
	if err != nil {
		// Validation error here is safe to return
		rw.WriteHeader(http.StatusBadRequest)
		io.WriteString(rw, err.Error())
		return
	}

	err = cs.SoldCars.Sell(soldCar)
	if err == ErrCarAlreadySold {
		// Sentinel error here is safe to return
		rw.WriteHeader(http.StatusConflict)
		io.WriteString(rw, err.Error())
		return
	} else if err != nil {
		log.Println("failed save sold car data, err is", err.Error())
		rw.WriteHeader(http.StatusInternalServerError)
		io.WriteString(rw, "failed to save sold car data")
		return
	}
}

func (cs *CarStoreHandler) Get(rw http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		rw.WriteHeader(http.StatusMethodNotAllowed)
		io.WriteString(rw, "incorrect method")
		return
	}

	serialNumberRaw := r.URL.Query().Get("serialNumber")
	if serialNumberRaw == "" {
		rw.WriteHeader(http.StatusBadRequest)
		io.WriteString(rw, "empty serialNumber")
		return
	}

	serialNumber, err := strconv.ParseUint(serialNumberRaw, 10, 64)
	if err != nil {
		rw.WriteHeader(http.StatusBadRequest)
		io.WriteString(rw, "incorrect serialNumber")
		return
	}

	car, err := cs.SoldCars.Get(serialNumber)
	if err == ErrCarDoesntExist {
		// Sentinel error here is safe to return
		rw.WriteHeader(http.StatusNotFound)
		io.WriteString(rw, err.Error())
		return
	}

	bytes, err := json.Marshal(&car)
	if err != nil {
		log.Println("failed marshal car data, err is", err.Error(), "car serial number is", car.SerialNumber)
		rw.WriteHeader(http.StatusInternalServerError)
		io.WriteString(rw, "failed to get sold car data")
		return
	}

	_, err = rw.Write(bytes)
	if err != nil {
		log.Println("failed write car data, err is", err.Error(), "car serial number is", car.SerialNumber)
		return
	}
}

func IsValid(car *Car) error {
	switch {
	case car.OwnerName == "":
		return errors.New("field ownerName is invalid")
	case car.SerialNumber == 0:
		return errors.New("field serialNumber is invalid")
	case car.ModelYear == 0:
		return errors.New("field modelYear is invalid")
	case car.Code == "":
		return errors.New("field code is invalid")
	case car.VehicleCode == "":
		return errors.New("field vehicleCode is invalid")
	case car.Manufacturer == "":
		return errors.New("field manufacturer is invalid")
	case car.Model == "":
		return errors.New("field model is invalid")
	case car.ActivationCode == "":
		return errors.New("field activationCode is invalid")
	case car.PerformanceFigures.OctaneRating == 0:
		return errors.New("field octaneRating is invalid")
	case car.PerformanceFigures.Acceleration.MPH == 0:
		return errors.New("field acceleration.mph is invalid")
	case car.PerformanceFigures.Acceleration.Seconds == 0 ||
		math.IsNaN(car.PerformanceFigures.Acceleration.Seconds) ||
		math.IsInf(car.PerformanceFigures.Acceleration.Seconds, 0):
		return errors.New("field acceleration.seconds is invalid")
	case car.FuelFigures.MPG == 0 ||
		math.IsNaN(car.FuelFigures.MPG) ||
		math.IsInf(car.FuelFigures.MPG, 0):
		return errors.New("field mpg is invalid")
	case car.FuelFigures.UsageDescription == "":
		return errors.New("field usageDescription is invalid")
	case car.FuelFigures.Speed == 0:
		return errors.New("field speed is invalid")
	case car.Engine.NumCylinders == 0:
		return errors.New("field numCylinders is invalid")
	case car.Engine.MaxRpm == 0:
		return errors.New("field maxRpm is invalid")
	case car.Engine.Capacity == 0:
		return errors.New("field capacity is invalid")
	case car.Engine.ManufacturerCode == 0:
		return errors.New("field manufacturerCode is invalid")
	}

	return nil
}

type Car struct {
	OwnerName          string                 `json:"ownerName"`
	SerialNumber       uint64                 `json:"serialNumber"`
	ModelYear          uint64                 `json:"modelYear"`
	Code               string                 `json:"code"`
	VehicleCode        string                 `json:"vehicleCode"`
	Manufacturer       string                 `json:"manufacturer"`
	Model              string                 `json:"model"`
	ActivationCode     string                 `json:"activationCode"`
	PerformanceFigures PerformanceFiguresData `json:"performanceFigures"`
	FuelFigures        FuelFiguresData        `json:"fuelFigures"`
	Engine             EngineData             `json:"engine"`
}

type EngineData struct {
	NumCylinders     uint16 `json:"numCylinders"`
	MaxRpm           uint16 `json:"naxRpm"`
	Capacity         uint16 `json:"capacity"`
	ManufacturerCode uint8  `json:"manufacturerCode"`
}

type FuelFiguresData struct {
	MPG              float64 `json:"mpg"`
	UsageDescription string  `json:"usageDescription"`
	Speed            uint16  `json:"speed"`
}

type PerformanceFiguresData struct {
	OctaneRating uint16 `json:"octaneRating"`
	Acceleration struct {
		Seconds float64 `json:"seconds"`
		MPH     uint16  `json:"mph"`
	} `json:"acceleration"`
}
