# Car service

Launch with 
```bash
go build ./cmd/server && ./server
```

Test sell with:

```bash
curl --request POST \
  --url http://localhost:8089/sell \
  --header 'content-type: application/json' \
  --data '{
	"ownerName": "Alex Denton",
	"serialNumber": 5002056,
	"modelYear": 2018,
	"code": "some code",
	"vehicleCode": "another code",
	"manufacturer": "LADA",
	"model": "PHANTOM",
	"activationCode": "Laputan Machine",
	"performanceFigures": {
		"octaneRating": 95,
		"acceleration": {
			"seconds": 9.5,
			"mph": 300
		}
	},
	"fuelFigures": {
		"mpg": 300.0,
		"usageDescription": "some description",
		"speed": 300
	},
	"engine": {
		"numCylinders": 20,
		"naxRpm": 3000,
		"capacity": 700,
		"manufacturerCode": 127
	}
}'
```

Test get with:
```bash
curl --request GET \
  --url 'http://localhost:8089/get?serialNumber=5002056'
```
