package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"bitbucket.org/Encriptic/dell-emc-auto"
)

func main() {
	storage, err := dell_emc_auto.NewSimpleStorage("./serverStorage.data")
	if err != nil {
		log.Fatal("failed to create storage ", err)
	}

	handler := dell_emc_auto.CarStoreHandler{
		SoldCars: storage,
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/sell", handler.Sell)
	mux.HandleFunc("/get", handler.Get)

	srv := &http.Server{
		Addr:         ":8089",
		Handler:      mux,
		ReadTimeout:  time.Minute,
		WriteTimeout: time.Minute,
	}

	log.Println("starting server")
	go func() {
		intCh := make(chan os.Signal, 10)
		signal.Notify(intCh, os.Interrupt)
		<-intCh
		ctx := context.Background()
		ctx, cancel := context.WithTimeout(ctx, time.Minute)
		defer cancel()
		err := srv.Shutdown(ctx)
		if err != nil {
			log.Println("failed to gracefully shutdown server. forcing. err is ", err.Error())
		}
		srv.Close()
	}()

	err = srv.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		log.Fatal("server returned an error", err)
	}

	log.Println("server stopped. bye bye!")
}
